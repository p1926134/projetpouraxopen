package Vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Modele.Jeu;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class VueControleurGrille extends JFrame implements Observer{
    private static final int PIXEL_PER_SQUARE = 60;
    // tableau de cases : i, j -> case
    private VueCase[][] tabCV;
    // hashmap : case -> i, j
    private HashMap<VueCase, Point> hashmap; // voir (*)
    // currentComponent : par défaut, mouseReleased est exécutée pour le composant (JLabel) sur lequel elle a été enclenchée (mousePressed) même si celui-ci a changé
    // Afin d'accéder au composant sur lequel le bouton de souris est relaché, on le conserve avec la variable currentComponent à
    // chaque entrée dans un composant - voir (**)
    private JComponent currentComponent;
    private Jeu game;
    private JMenu aideMenu, textureMenu;
    private JMenuItem refreshButton , textureButton1,textureButton2 ;

    public VueControleurGrille(int size, Jeu jeu) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize((size) * PIXEL_PER_SQUARE, (size) * PIXEL_PER_SQUARE);
        tabCV = new VueCase[size][size];
        hashmap = new HashMap<VueCase, Point>();
        game = jeu; 

        JPanel contentPane = new JPanel(new GridLayout(size, size));
        JMenuBar menubar = new JMenuBar();
        aideMenu = new JMenu("Aide");
        refreshButton = new JMenuItem("Refresh la page");
        aideMenu.add(refreshButton);
        
        textureMenu = new JMenu("Texture");
        textureButton1 = new JMenuItem("Texture 1");
        textureButton2 = new JMenuItem("Texture 2");
        textureMenu.add(textureButton1);
        textureMenu.add(textureButton2);
        
        menubar.add(textureMenu);
        menubar.add(aideMenu);
        
        this.setJMenuBar(menubar);
        
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.init();
            }
        });
        textureButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < size; i++) {
                    for (int j = 0; j < size; j++) { 
                        tabCV[i][j].setNbTexture(1);
                        refresh();
                    }
                }     
            }
        });
        textureButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < size; i++) {
                    for (int j = 0; j < size; j++) { 
                        tabCV[i][j].setNbTexture(2);
                        refresh();
                    }
                }     
            }
        });

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                tabCV[i][j] = new VueCase(i, j);
                contentPane.add(tabCV[i][j]);

                hashmap.put(tabCV[i][j], new Point(j, i));

                tabCV[i][j].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        Point p = hashmap.get(e.getSource()); // (*) permet de récupérer les coordonnées d'une caseVue
                        game.onClick(p);
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        Point p = hashmap.get(e.getSource());
                        game.slide(p);
                        currentComponent = (JComponent) e.getSource();
                    }   

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        game.offClick();
                    }
                });
            }
        }
        setContentPane(contentPane);

    }
    
    @Override
    public void update(Observable obs, Object obj){
        refresh();
        if(game.victoire()){
            victoireShow();
        }
    }
    
    public void refresh(){
        for (int i = 0 ; i <tabCV.length ; i++){
           for (int j = 0 ; j <tabCV[0].length ; j++){
               tabCV[i][j].setType(game.getTabCase(i, j).getType());
               repaint();
           }
        }
    }

    private void victoireShow(){
        int n = game.getNiveau() - 1;
        JFrame frame = new JFrame("showMessageDialog");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        if (n < 3){
            JOptionPane.showMessageDialog(frame,"Niveau "+ n + " fini");
            
        }else{
            JOptionPane.showMessageDialog(frame,"Partie terminé");
            System.exit(n);
        }
        game.init();
    }

    public static void main(String[] args) {
        Jeu jeu = new Jeu(5);    
        VueControleurGrille vue = new VueControleurGrille(5,jeu);
        vue.refresh();
        jeu.addObserver(vue);
        vue.setVisible(true); 
    }

}
