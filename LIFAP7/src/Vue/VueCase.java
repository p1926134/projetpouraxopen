package Vue;


import Modele.CaseType;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;


// TODO : redéfinir la fonction hashValue() et equals(Object) si vous souhaitez utiliser la hashmap de VueControleurGrille avec VueCase en clef

public class VueCase extends JPanel {
    private int x, y;
    public CaseType type;
    private int nbTexture;

    public VueCase(int _x, int _y) {
        this.x = _x;
        this.y = _y;
        this.nbTexture = 1;
    }

    public void setNbTexture(int nbTexture) {
        this.nbTexture = nbTexture;
    }

    private void drawNoon(Graphics g) {
        g.fillRect(getWidth()/2 - 5, 0, 10, getHeight()/2);
    }

    private void drawNine(Graphics g) {
        g.fillRect(0, getHeight()/2 - 5, getWidth()/2, 10);
    }

    private void drawSix(Graphics g) {
        g.fillRect(getWidth()/2 - 5, getHeight()/2, 10, getHeight()/2);
    }

    private void drawThree(Graphics g) {
        g.fillRect(getWidth()/2, getHeight()/2 - 5, getWidth(), 10);
    }
    
     private void drawLineNoon(Graphics g) {
         g.drawLine(getWidth()/2, getHeight()/2, getWidth()/2, 0);
    }

    private void drawLineNine(Graphics g) {
         g.drawLine(0, getHeight()/2, getWidth()/2, getHeight()/2);
    }

    private void drawLineSix(Graphics g) {
        g.drawLine(getWidth()/2, getHeight()/2, getWidth()/2, getHeight());
    }

    private void drawLineThree(Graphics g) {
        g.drawLine(getWidth()/2, getHeight()/2, getWidth(), getHeight()/2);
    }



    @Override
    public void paintComponent(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());

        switch(nbTexture) {
            case 1 : 
            g.drawRoundRect(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2, 5, 5);

            Rectangle2D deltaText =  g.getFont().getStringBounds("0", g.getFontMetrics().getFontRenderContext()); // "0" utilisé pour gabarit
            switch(type) {
                case S1 :
                    g.drawString("1", getWidth()/2 - (int) deltaText.getCenterX(), getHeight()/2 - (int) deltaText.getCenterY());
                    break;
                case S2 :
                    g.drawString("2", getWidth()/2  - (int) deltaText.getCenterX(), getHeight()/2 - (int) deltaText.getCenterY());
                    break;
                case S3 :
                    g.drawString("3", getWidth()/2  - (int) deltaText.getCenterX(), getHeight()/2 - (int) deltaText.getCenterY());
                    break;
                case S4 :
                    g.drawString("4", getWidth()/2  - (int) deltaText.getCenterX(), getHeight()/2 - (int) deltaText.getCenterY());
                    break;
                case S5 :
                    g.drawString("5", getWidth()/2  - (int) deltaText.getCenterX(), getHeight()/2 - (int) deltaText.getCenterY());
                    break;
                case h0v0 :
                    drawLineNine(g);
                    drawLineNoon(g);
                    break;
                case h0v1 :
                    drawLineNine(g);
                    drawLineSix(g);
                    break;
                case h1v0:
                    drawLineNoon(g);
                    drawLineThree(g);
                    break;
                case h1v1 :
                    drawLineThree(g);
                    drawLineSix(g);
                    break;
                case h0h1:
                    drawLineThree(g);
                    drawLineNine(g);
                    break;
                case v0v1:
                    drawLineNoon(g);
                    drawLineSix(g);
                    break;
                case cross:
                    drawLineNoon(g);
                    drawLineSix(g);
                    drawLineThree(g);
                    drawLineNine(g);
                    break;
                case empty:
                    break;
            }
            break;
            case 2 :
            g.drawRoundRect(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2, 5, 5);
            switch(type) {
                case S1 :
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.setColor(Color.red);
                    g.fillOval(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2);
                    break;
                case S2 :
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.setColor(Color.black);
                    g.fillOval(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2);
                    break;
                case S3 :
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.setColor(Color.blue);
                    g.fillOval(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2);
                    break;
                case S4 :
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.setColor(Color.green);
                    g.fillOval(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2);
                    break;
                case S5 :
                    g.clearRect(0, 0, getWidth(), getHeight());
                    g.setColor(Color.yellow);
                    g.fillOval(getWidth()/4, getHeight()/4, getWidth()/2, getHeight()/2);
                    break;
                case h0v0 :
                    drawNine(g);
                    drawNoon(g);
                    break;
                case h0v1 :
                    drawNine(g);
                    drawSix(g);
                    break;
                case h1v0:
                    drawNoon(g);
                    drawThree(g);
                    break;
                case h1v1 :
                    drawThree(g);
                    drawSix(g);
                    break;
                case h0h1:
                    drawThree(g);
                    drawNine(g);
                    break;
                case v0v1:
                    drawNoon(g);
                    drawSix(g);
                    break;
                case cross:
                    drawNoon(g);
                    drawSix(g);
                    drawThree(g);
                    drawNine(g);
                    break;
                case empty:
                    break;
            } 
            break;
        }
    }
            
    

    public void setType(CaseType type) {
        this.type = type;
    }
    
    
    public String toString() {
        return x + ", " + y;

    }
    
    

}
