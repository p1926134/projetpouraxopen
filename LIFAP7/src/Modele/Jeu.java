/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

// import com.sun.webkit.dom.MouseEventImpl;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Set;


/**
 *
 * @author mumuy
 */
public class Jeu extends Observable {
    private ModeleCase[][] tabCase;
    private List<Chemin> listChemin;
    private boolean clicked;
    private int niveau;

    public Jeu(int size) {
        this.tabCase = new ModeleCase[size][size];
        this.listChemin = new ArrayList<Chemin>();
        this.clicked = false;
        this.niveau = 1;
        init();
    }
    
    public void init(){
        for(int i = 0;i<tabCase.length;i++){
            for(int j = 0;j<tabCase.length;j++){
                tabCase[i][j] = new ModeleCase(i, j) ;
            }
        }
        //HardCode niv 1
        if(niveau == 1){
            tabCase[0][0].setType(CaseType.S1);
            tabCase[1][4].setType(CaseType.S1);
            tabCase[1][0].setType(CaseType.S2);
            tabCase[4][4].setType(CaseType.S2);
            tabCase[4][3].setType(CaseType.S3);
            tabCase[2][2].setType(CaseType.S3);
            tabCase[3][1].setType(CaseType.S4);
            tabCase[3][3].setType(CaseType.S4);
        }
        
        //HardCode niv 2
        if(niveau ==2 ){
            tabCase[1][2].setType(CaseType.S1);
            tabCase[3][3].setType(CaseType.S1);
            tabCase[3][1].setType(CaseType.S2);
            tabCase[4][4].setType(CaseType.S2);
            tabCase[0][0].setType(CaseType.S3);
            tabCase[1][4].setType(CaseType.S3);
            tabCase[1][3].setType(CaseType.S4);
            tabCase[3][4].setType(CaseType.S4);
        }

        //HardCode niv 3
        if(niveau ==3 ){
            tabCase[4][0].setType(CaseType.S1);
            tabCase[4][4].setType(CaseType.S1);
            tabCase[3][1].setType(CaseType.S2);
            tabCase[1][4].setType(CaseType.S2);
            tabCase[2][1].setType(CaseType.S3);
            tabCase[0][4].setType(CaseType.S3);
            tabCase[1][1].setType(CaseType.S4);
            tabCase[3][0].setType(CaseType.S4);
        }

        setChanged();
        notifyObservers();
    }
    
    public ModeleCase getTabCase(int i, int j) {
        return tabCase[i][j];
    }

    public int getNiveau(){
        return niveau;
    }

    private boolean verifyCase(int _x, int _y){
        return tabCase[_y][_x].getType() == CaseType.S1 || tabCase[_y][_x].getType() == CaseType.S2 
                || tabCase[_y][_x].getType() == CaseType.S3 || tabCase[_y][_x].getType() == CaseType.S4 
                || tabCase[_y][_x].getType() == CaseType.S5;
    }

    public void onClick(Point p){
        if(verifyCase(p.x,p.y)){
            if (!tabCase[p.x][p.y].getComplet()){
                this.Debut(p);
            if(tabCase[p.x][p.y].getComplet()){} 
                for(int i=0; i<listChemin.size();i++){
                    List<ModeleCase> chemin = listChemin.get(i).getListCase();
                    if((chemin.get(0).getX()==p.x && chemin.get(0).getY()==p.y) ||
                        (chemin.get(chemin.size()-1).getX()==p.x && chemin.get(chemin.size()-1).getY()==p.y)){
                            listChemin.get(i).detruireChemin();
                            listChemin.remove(i);
                            setChanged();
                            notifyObservers();
                            this.Debut(p);
                    }
                }       
            }
        }
    }

    private void Debut(Point p){
        listChemin.add(new Chemin());
        clicked = true;
        listChemin.get(listChemin.size()-1).DebutChemin(tabCase[p.y][p.x]);
    }
    
    public void slide(Point p){
        if(clicked){
            if(listChemin.get(listChemin.size()-1).getLongueur()==0 && !(verifyCase(p.x, p.y))){
                if(!listChemin.get(listChemin.size()-1).AjouterCase(tabCase[p.y][p.x]))
                    this.offClick();
            }else if (listChemin.get(listChemin.size()-1).getLongueur()!=0 ){
                if(!listChemin.get(listChemin.size()-1).AjouterCase(tabCase[p.y][p.x]))
                    this.offClick();
            }
            setChanged();
            notifyObservers();
            
        }
    }

    public void offClick(){   
        if(!listChemin.get(listChemin.size()-1).validerChemin()){
            listChemin.remove(listChemin.size()-1);
        }
        setChanged();
        notifyObservers();
        clicked = false;
    }

    public boolean victoire(){
        for(int i=0; i<tabCase.length; i++){
            for(int j=0; j<tabCase.length; j++){
                if (tabCase[j][i].getType() == CaseType.empty){
                    return false;
                }
                if (verifyCase(i,j) && !tabCase[j][i].getComplet()){
                    return false;
                }
            }
        }
        if(niveau<4){
            niveau++;
        }
        return true;
    }
}
