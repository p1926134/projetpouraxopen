/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author mumuy
 */
public class Chemin{
    private List<ModeleCase> listCase;
    private CaseType FType;

    public Chemin() {
        listCase = new ArrayList<ModeleCase>();
    }
    
    public int getLongueur(){
        return listCase.size();
    }

    public List<ModeleCase> getListCase() {
        return listCase;
    }


    public void DebutChemin(ModeleCase p){
        listCase.add(p);
        FType = p.getType();
    }
    
    public boolean AjouterCase(ModeleCase p){

        if(p.getType() != CaseType.empty && !verify(p)){
            return false;
        }
        
        ModeleCase precedant = listCase.get(listCase.size() - 1);
        int diffX = p.getX() - precedant.getX();
        int diffY = p.getY() - precedant.getY();

        if(diffX != 0 && diffY != 0){
            return false;
        }

        if(diffX!=0){
            if(diffX == 1){
                if(!verify(p))
                    p.setType(CaseType.h0h1);
                if(listCase.size()>1){
                    diffY = precedant.getY() - listCase.get(listCase.size()-2).getY();
                    if(diffY==1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h1v0);
                    }
                    if(diffY==-1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h1v1);
                    }
                }
            }
            if(diffX == -1){
                if(!verify(p))
                    p.setType(CaseType.h0h1);
                if(listCase.size()>1){
                    diffY = precedant.getY() - listCase.get(listCase.size()-2).getY();
                    if(diffY==1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h0v0);
                    }
                    if(diffY==-1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h0v1);
                    }
                }
            }
        }
        
        diffX = p.getX() - precedant.getX();
        diffY = p.getY() - precedant.getY();
        
        if(diffY!=0){
            if(diffY == 1){
                if(!verify(p))
                    p.setType(CaseType.v0v1);
                if(listCase.size()>1){
                    diffX = precedant.getX() - listCase.get(listCase.size()-2).getX();
                    if(diffX==1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h0v1);
                    }
                    if(diffX==-1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h1v1);
                        
                    }
                }
            }
            
            if(diffY == -1){
                if(!verify(p))
                    p.setType(CaseType.v0v1);
                if(listCase.size()>1){
                    diffX = precedant.getX() - listCase.get(listCase.size()-2).getX();
                    if(diffX==1){
                        listCase.get(listCase.size() - 1).setType(CaseType.h0v0);   
                    }
                    if(diffX==-1){
                        
                        listCase.get(listCase.size() - 1).setType(CaseType.h1v0);        
                    }  
                } 
            }
        }
        listCase.add(p);
        return true;
        
    }

    public void detruireChemin(){
        for(int i = 0; i<listCase.size();i++){
            if(!(verify(listCase.get(i)))){
                listCase.get(i).setType(CaseType.empty);
            }
        }
        listCase.get(0).setComplet(false);
        listCase.get(listCase.size()-1).setComplet(false);
        listCase.clear();
    }
    
    public boolean validerChemin(){
        if(FType != listCase.get(listCase.size()-1).getType()){
            
           this.detruireChemin();
           return false;
        }
        listCase.get(0).setComplet(true);
        listCase.get(listCase.size()-1).setComplet(true);
        return true;
    }


    
    private boolean verify(ModeleCase m){
        return m.getType() == CaseType.S1 ||
               m.getType() == CaseType.S2 || 
               m.getType() == CaseType.S3 || 
               m.getType() == CaseType.S4 || 
               m.getType() == CaseType.S5;
    }
    
}
    
    
    
    
    
    

