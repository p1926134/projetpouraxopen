/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;


/**
 *
 * @author mumuy
 */
public class ModeleCase {
    
    private int x,y;
    private CaseType type;
    private boolean complet;

    public ModeleCase(int x, int y) {
        this.x = x;
        this.y = y;
        complet = false;
        type = CaseType.empty;
    }    
    
    public CaseType getType() {
        return type;
    }

    public void setType(CaseType type) {
        this.type = type;
    }

    public boolean getComplet(){
        return complet;
    }

    public void setComplet(boolean _complet) {
        this.complet = _complet;
    }

    public int getX(){
        return y;
    }

    public int getY(){
        return x;
    }

    
    
}
